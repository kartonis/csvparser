cmake_minimum_required(VERSION 3.12)
project(csv)

set(CMAKE_CXX_STANDARD 14)
add_subdirectory(googletest-master)
include_directories(googletest-master/googletest/include)
include_directories(googletest-master/googlemock/include)
add_executable(csv main.cpp csv.h)
add_executable(Tests Test.cpp csv.h)



target_link_libraries(Tests gtest gtest_main)