#include "csv.h"
#include "gtest/gtest.h"

TEST(Print_tuple, Case1)
{
    std::tuple<int, std::string> case1={1, "Andrey"};
    std::stringstream ss;
    ss<<case1;
    EXPECT_EQ(ss.str(),"1 Andrey\n");
}
TEST(Read_data, Case2)
{
    std::ifstream input("in.txt");
    CSVParser <int, std::string> parser(input);
    std::stringstream ss;
    for (auto t : parser)
    {
        ss << t;
    }
    EXPECT_EQ(ss.str(), "1 Andrey\n2 Vika\n3 Queen;\n");
}
TEST(String_to_tuple, Case3)
{
    std::vector<std::string> Vector={"string", "123", "a", "8.0"};
    std::tuple<std::string, int, char, double> range;
    TuplePacker<decltype(range), 4>::pack(range, Vector);
    EXPECT_EQ(std::get<0>(range), "string");
    EXPECT_EQ(std::get<1>(range), 123);
    EXPECT_EQ(std::get<2>(range), 'a');
    EXPECT_EQ(std::get<3>(range), 8.0);
}

TEST(Exception, Case4)
{
    std::vector<std::string> Vector={"string"};
    std::tuple<int> range;
    try
    {
        TuplePacker<decltype(range), 1>::pack(range, Vector);
        EXPECT_EQ(0,1);
    }
    catch(std::invalid_argument&)
    {
        EXPECT_EQ(1,1);
    }
}
int main(int argc, char** argv)
{
    testing::InitGoogleTest(&argc, argv);
    RUN_ALL_TESTS();
    return 0;
}
